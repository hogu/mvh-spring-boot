package be.gvh.mvh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import be.gvh.mvh.domain.Kalender;

public interface KalenderRepository extends CrudRepository<Kalender, Long> {

	@Query("select k from Kalender k order by k.jaar")
	List<Kalender> findAllOrderByJaar();

	@Query("select k from Kalender k order by k.jaar desc")
	List<Kalender> findAllOrderByJaarDescending();

}
