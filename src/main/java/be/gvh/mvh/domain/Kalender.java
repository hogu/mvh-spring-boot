package be.gvh.mvh.domain;

import java.time.Year;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "KALENDERS")
public class Kalender {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "KALENDER_ID")
	private Long id;
	@Column(name = "KALENDER_JAAR", unique = true)
	private Year jaar;
	@Column(name = "KALENDER_OPMERKINGEN")
	private String opmerkingen;
	@Version
	@Column(name = "KALENDER_VERSIE")
	private Long versie;

	public Kalender() {
	}

	public Kalender(Year jaar) {
		setId(0L);
		setJaar(jaar);
		setVersie(0L);
	}

	public Kalender(Long kalenderId, Year jaar) {
		setId(kalenderId);
		setJaar(jaar);
		setVersie(0L);
	}

	public Kalender(Long kalenderId, Year jaar, String opmerkingenKalender) {
		setId(kalenderId);
		setJaar(jaar);
		setOpmerkingen(opmerkingenKalender);
		setVersie(0L);
	}

	public void setId(Long kalenderId) {
		this.id = kalenderId;
	}

	public Long getId() {
		return this.id;
	}

	public void setJaar(Year jaar) {
		this.jaar = jaar;
	}

	public Year getJaar() {
		return this.jaar;
	}

	public void setOpmerkingen(String opmerkingenKalender) {
		this.opmerkingen = opmerkingenKalender;
	}

	public String getOpmerkingen() {
		return this.opmerkingen;
	}

	public void setVersie(long versie) {
		this.versie = versie;
	}

	public Long getVersie() {
		return this.versie;
	}

	@Override
	public String toString() {
		return "Wedstrijdkalender " + jaar;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((jaar == null) ? 0 : jaar.hashCode());
		result = prime * result + ((opmerkingen == null) ? 0 : opmerkingen.hashCode());
		result = prime * result + ((versie == null) ? 0 : versie.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kalender other = (Kalender) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (jaar == null) {
			if (other.jaar != null)
				return false;
		} else if (!jaar.equals(other.jaar))
			return false;
		if (opmerkingen == null) {
			if (other.opmerkingen != null)
				return false;
		} else if (!opmerkingen.equals(other.opmerkingen))
			return false;
		if (versie == null) {
			if (other.versie != null)
				return false;
		} else if (!versie.equals(other.versie))
			return false;
		return true;
	}	
}
