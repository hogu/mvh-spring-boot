package be.gvh.mvh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvhSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvhSpringBootApplication.class, args);
	}
}
