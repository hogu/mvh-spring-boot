package be.gvh.mvh.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import be.gvh.mvh.config.H2Configuration;
import be.gvh.mvh.domain.Kalender;

@ContextConfiguration(classes = H2Configuration.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class KalenderRepositoryInegrationTest {
	private static final Year VOLGEND_JAAR = Year.now().plusYears(1);
	private static final Year HUIDIG_JAAR = Year.now();
	private static final Year VORIG_JAAR = HUIDIG_JAAR.plusYears(-1);
	private List<Kalender> kalenders = new ArrayList<>();
	private List<Kalender> expectedKalenders = new ArrayList<>();
	private List<Kalender> actualKalenders = new ArrayList<>();
	@Autowired
	private KalenderRepository kalenderRepository;

	@Test
	public void integrationTest() {
		maakKalenders();
		bewaarNieuweKalenders();
		leesKalenders();
		controleerKalenders();
		wijzigKalenders();
		bewaarBestaandeKalenders();
		leesKalenders();
		controleerKalenders();
		leesKalendersOpJaar();
		controleerKalendersOpJaar();
		leesKalendersOpJaarOplopend();
		controleerKalendersOpJaarOplopend();
		verwijderKalenders();
		leesKalenders();
		controleerVerwijderdeKalenders();
	}

	private void maakKalenders() {
		kalenders.add(new Kalender(VORIG_JAAR));
		kalenders.add(new Kalender(HUIDIG_JAAR));
		kalenders.add(new Kalender(VOLGEND_JAAR));
	}

	private void bewaarNieuweKalenders() {
		for (Kalender kalender : kalenders) {
			expectedKalenders.add(kalenderRepository.save(kalender));
		}
	}

	private void leesKalenders() {
		actualKalenders.clear();
		for (Kalender expectedKalender : expectedKalenders) {
			actualKalenders.add(kalenderRepository.findOne(expectedKalender.getId()));
		}
	}

	private void controleerKalenders() {
		int aantalKalenders = expectedKalenders.size();
		assertEquals(aantalKalenders, actualKalenders.size());
		for (int index = 0; index < aantalKalenders; index++) {
			assertEquals(expectedKalenders.get(index), actualKalenders.get(index));
		}
	}

	private void leesKalendersOpJaar() {
		actualKalenders.clear();
		actualKalenders = kalenderRepository.findAllOrderByJaar();
	}

	private void controleerKalendersOpJaar() {
		Year[] expectedJaren = { VORIG_JAAR, HUIDIG_JAAR, VOLGEND_JAAR };
		int index = 0;
		for (index = 0; index < actualKalenders.size(); index++) {
			assertEquals(actualKalenders.get(index).getJaar(), expectedJaren[index]);
		}
	}

	private void leesKalendersOpJaarOplopend() {
		actualKalenders.clear();
		actualKalenders = kalenderRepository.findAllOrderByJaarDescending();
	}

	private void controleerKalendersOpJaarOplopend() {
		Year[] expectedJaren = { VOLGEND_JAAR, HUIDIG_JAAR, VORIG_JAAR };
		int index = 0;
		for (index = 0; index < actualKalenders.size(); index++) {
			assertEquals(actualKalenders.get(index).getJaar(), expectedJaren[index]);
		}
	}

	private void wijzigKalenders() {
		for (int index = 0; index < expectedKalenders.size(); index++) {
			Kalender kalender = expectedKalenders.get(index);
			kalender.setOpmerkingen("Opmerkingen kalender" + kalender.getJaar().toString());
			expectedKalenders.set(index, kalenderRepository.save(kalender));
		}
	}

	private void bewaarBestaandeKalenders() {
		for (Kalender expectedKalender : expectedKalenders) {
			kalenderRepository.save(expectedKalender);
		}
	}

	private void verwijderKalenders() {
		for (Kalender expectedKalender : expectedKalenders) {
			kalenderRepository.delete(expectedKalender);
		}
	}

	private void controleerVerwijderdeKalenders() {
		for (Kalender actualKalender : actualKalenders) {
			assertNull(actualKalender);
		}
	}
}
