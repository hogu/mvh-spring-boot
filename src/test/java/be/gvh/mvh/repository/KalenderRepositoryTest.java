package be.gvh.mvh.repository;

import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class KalenderRepositoryTest {

	@Mock
	private KalenderRepository kalenderRepository;

	@Test
	public void vindAlleGesorteerdOpJaar() {
		kalenderRepository.findAllOrderByJaar();
		verify(kalenderRepository).findAllOrderByJaar();
	}

	@Test
	public void vindAlleGesorteerdOpJaarOplopend() {
		kalenderRepository.findAllOrderByJaarDescending();
		verify(kalenderRepository).findAllOrderByJaarDescending();
	}
}
