package be.gvh.mvh.domain;

import static org.junit.Assert.*;

import java.time.Year;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class KalenderTest {
	private static final String OPMERKINGEN_KALENDER = "Opmerkingen kalender";
	private static final Long KALENDER_ID_1 = 1L;
	private static final Year HUIDIG_JAAR = Year.now();
	private static final String KALENDER_STRING = "Wedstrijdkalender " + HUIDIG_JAAR;
	private Kalender kalender;

	@Test
	public void testConstructorZonderArgumenten() {
		kalender = new Kalender();
		assertNull(kalender.getId());
		assertNull(kalender.getJaar());
		assertNull(kalender.getOpmerkingen());
		assertNull(kalender.getVersie());		
	}

	@Test
	public void testConstructorMet1Argument() {
		kalender = new Kalender(HUIDIG_JAAR);
		assertEquals(new Long(0), kalender.getId());
		assertEquals(HUIDIG_JAAR, kalender.getJaar());
		assertNull(kalender.getOpmerkingen());
		assertEquals(new Long(0),kalender.getVersie());
	}

	@Test
	public void testConstructorMet2Argumenten() {
		kalender = new Kalender(KALENDER_ID_1, HUIDIG_JAAR);
		assertEquals(KALENDER_ID_1, kalender.getId());
		assertEquals(HUIDIG_JAAR, kalender.getJaar());
		assertNull(kalender.getOpmerkingen());
		assertEquals(new Long(0),kalender.getVersie());
	}
	
	@Test
	public void testConstructorMet3Argumenten() {
		kalender = new Kalender(KALENDER_ID_1, HUIDIG_JAAR, OPMERKINGEN_KALENDER);
		assertEquals(KALENDER_ID_1, kalender.getId());
		assertEquals(HUIDIG_JAAR, kalender.getJaar());
		assertEquals(OPMERKINGEN_KALENDER, kalender.getOpmerkingen());
		assertEquals(new Long(0),kalender.getVersie());
	}

	@Test
	public void kalenderToString() {
		kalender = new Kalender(KALENDER_ID_1, HUIDIG_JAAR, OPMERKINGEN_KALENDER);
		assertEquals(KALENDER_STRING, kalender.toString());
	}	
}
